// Разработка

// import './src/instrum/libs/develop.js';
// import './src/instrum/dev.styl';

// Разработка

// Плагины

import "font-awesome/css/font-awesome.min.css";
import 'owl.carousel/dist/owl.carousel.min.js';
import 'owl.carousel/dist/assets/owl.carousel.min.css';
import 'owl.carousel/dist/assets/owl.theme.default.min.css';
// import 'jquery.maskedinput/src/jquery.maskedinput.js';
import 'jquery-validation/dist/jquery.validate.min.js';
// import 'custom-scroll/source/jquery.custom-scroll.css';
// import 'custom-scroll/source/jquery.custom-scroll.js';
// import "./src/instrum/popap.styl";
// import './src/instrum/libs/popap.js';

import "./src/instrum/reset.styl";
import "./src/instrum/framework.styl";

// import './src/instrum/libs/tabs.js';
// import './src/instrum/libs/scroll-to.js';
// import './src/instrum/libs/menu.js';
import './src/instrum/libs/filter.js';

// Плагины

// Шрифты

// import "./src/fonts/...";

if (module.hot) {
    module.hot.accept();
}

$('.let').on('click', function() {
    $('.letter').fadeIn();
    $('#wrap').animate({
        top: '50%'
    }, 700)
})

$('.letter').on('click', function() {
    $('#wrap').animate({
        top: '150%'
    }, 700)
    $('.letter').fadeOut();
})


$('.nav_item').on('click', function () {
    if($( window ).width() < 750) {
    $('aside').css('display', 'none')
}
})

$(function() {
    $('form').validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                email: true
            },
            message: {
                required: true,
                minlength: 10
            }
        },
        messages: {
            name: {
                required: "Поле 'Имя' обязательно к заполнению",
                minlength: "Введите не менее 2-х символов в поле 'Имя'"
            },
            email: {
                required: "Поле 'Email' обязательно к заполнению",
                email: "Необходим формат адреса email"
            },
            message: {
                required: "Поле 'Сообщение' обязательно к заполнению",
                minlength: "Введите не менее 10-и символов"
            }
        }
    });
});

$('.burger').on('click', function () {
    $('aside').toggle()
})

$('.portfolio').mixItUp().sort();

// Обо мне

$('.niz').click(function(){
  $('.niz').toggleClass('active');
  $('.title').toggleClass('active');
  $('.nav-card').toggleClass('active');
});

// Подсчет работе

let attr = document.querySelectorAll('.filter_item');

for (var i = 0; i < attr.length; i++) {
    attr[i].children[0].innerHTML = $('.work'+ '' + attr[i].getAttribute('data-filter')).length;
    attr[0].children[0].innerHTML = $('.work').length;
}
