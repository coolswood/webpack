const common = require('./webpack.common.js');

const CleanWebpackPlugin = require('clean-webpack-plugin');
const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const glob = require('glob');
const PurifyCSSPlugin = require('purifycss-webpack');
const tinyPngWebpackPlugin = require('tinypng-webpack-plugin');

const extractSTYL = new ExtractTextPlugin({
    filename: (getPath) => {
        return getPath('css/[name].css').replace('css/js', 'css');
    },
    allChunks: true
});

const PATHS = {
    source: path.join(__dirname, 'src'),
    build: path.join(__dirname, 'build')
};

module.exports = merge(common, {
    module: {
        rules: [{
                test: /\.js$/,
                exclude: /node_modules/,
                use: [{
                    loader: "babel-loader",
                    options: {
                        "presets": ["env"]
                    }
                }]
            },

            {
                test: /\.(styl|css)$/,
                use: extractSTYL.extract({
                    publicPath: '../',
                    fallback: "style-loader",
                    use: ['css-loader', /*'group-css-media-queries-loader',*/ 'postcss-loader', 'stylus-loader']
                })
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                exclude: /node_modules/,
                use: [{
                        loader: 'file-loader',
                        options: {
                            name: 'img/[path][name].[ext]',
                            context: './src/pages/'
                        }
                    },
                    {
                        loader: 'image-webpack-loader', // Сжимаем картинки
                        options: {
                            optipng: {
                                enabled: false
                            },
                            pngquant: {
                                enabled: false
                            }
                        }
                    }
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                exclude: path.resolve(__dirname, "src"),
                use: [{
                        loader: 'file-loader',
                        options: {
                            name: 'img/[path][name].[ext]',
                            context: './node_modules/'
                        }
                    },
                    {
                        loader: 'image-webpack-loader', // Сжимаем картинки
                        options: {
                            optipng: {
                                enabled: false
                            },
                            pngquant: {
                                enabled: false
                            }
                        }
                    }
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                exclude: /node_modules/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: 'fonts/[path][name].[ext]',
                        context: './src/fonts'
                    }
                }]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                exclude: path.resolve(__dirname, "src"),
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: 'fonts/[path][name].[ext]',
                        context: './node_modules/'
                    }
                }]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(['build/*']),
        extractSTYL,
        new tinyPngWebpackPlugin({
            key:"9bdG3qBKDx79VRy6KNfEJK59vd1ptRhJ",
            ext: ['png']
        })
        // new PurifyCSSPlugin({
        //     paths: glob.sync(path.join(__dirname, 'src/pages/*/*.html'))
        // })

    ]
})
