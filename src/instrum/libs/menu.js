let container = '.nav-content';

$( window ).resize(function() {
  if($(window).width() > 950) {
      $(container).addClass('show')
      $(container).css('transform', 'translate(0, 0)')
  } else {
      $(container).removeClass('show')
      $(container).css('transform', 'translate(-120%, 0)').css('z-index', '100')
  }
});

$('.toggleMenu').on('click', function() {
    if( $(container).hasClass('show')) {
        $(container).removeClass('show')
        $(container).css('transform', 'translate(-120%, 0)').css('z-index', '100')
    } else {
        $(container).addClass('show')
        $(container).css('transform', 'translate(0, 0)')
    }

});
