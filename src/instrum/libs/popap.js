$('.popap-btn').on('click', function () {
    $('.popap-black').fadeIn();
    $('.popap').animate({
        top: '50%'
    }, 500)
})
$('.popap-black, .popap-close').on('click', function () {
    $('.popap-black').fadeOut();
    $('.popap').animate({
        top: '-50%'
    }, 500)
})
