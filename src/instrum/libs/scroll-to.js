$(document).ready(function() {
    $("a").click(function () {
      var elementClick = $(this).attr("href");
      var destination = $(elementClick).offset().top - 80;
      $('html,body').animate( { scrollTop: destination }, 1100 );
      return false;
    });
  });
