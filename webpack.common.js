const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const SpritesmithPlugin = require('webpack-spritesmith');

const PATHS = {
    source: path.join(__dirname, 'src'),
    build: path.join(__dirname, 'build')
};

module.exports = {
    entry: {
        main: PATHS.source + '/pages/__main/index'
    },
    output: {
        path: PATHS.build,
        filename: 'js/[name].js'
    },
    watchOptions: {
        aggregateTimeout: 100
    },
    module: {
        rules: [{
                test: /\.html$/,
                loader: 'html-loader',
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [{
                    loader: "babel-loader",
                        options: {
                        "presets": ["stage-0"]
                    }
                }]
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: [{
                    loader: "babel-loader",
                        options: {
                        presets: ["stage-0", "react"]
                    }
                }]
            }

        ]
    },
    plugins: [
        new webpack.ProvidePlugin({ // Подключает библиотеки автоматом
            $: 'jquery',
            jQuery: "jquery",
            "window.jQuery": "jquery",
            "isotope": "isotope-layout/dist/isotope.pkgd.js"
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            // chunks: ['main', 'common'],
            template: PATHS.source + '/pages/__main/index.html'
        }),
        new SpritesmithPlugin({
            src: {
                cwd: path.resolve(__dirname, 'src/pages/__main/img/sprite'),
                glob: '*.png'
            },
            target: {
                image: path.resolve(__dirname, 'src/pages/__main/img/sprite.png'),
                css: path.resolve(__dirname, 'src/pages/__main/style/sprite.css')
            },
            apiOptions: {
                cssImageRef: "../img/sprite.png"
            }
        })
    ]
};
