const common = require('./webpack.common.js');

const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const smartgrid = require('smart-grid');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

const PATHS = {
    source: path.join(__dirname, 'src'),
    build: path.join(__dirname, 'build')
};

module.exports = merge(common, {

    devtool: 'eval', // Карта
    devServer: {
        contentBase: PATHS.source,
        compress: true,
        port: 8080,
        inline: true,
        overlay: true
    },
    module: {
        rules: [{
                test: /\.styl$/,
                use: ['style-loader', 'css-loader', 'postcss-loader', 'stylus-loader']
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader'
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        context: './source/fonts'
                    }
                }]
            }
        ]
    },
    plugins: [
        new BrowserSyncPlugin({
            host: 'localhost',
            tunnel: true,
            port: 3000,
            proxy: 'http://localhost:8080/',
            files: [{
                match: [
                    '**/*.html'
                ],
                fn: function(event, file) {
                    if (event === "change") {
                        const bs = require('browser-sync').get('bs-webpack-plugin');
                        bs.reload();
                    }
                }
            }]
        }, {
            reload: false
        })
    ]
});

var settings = {
    outputStyle: 'styl',
    /* less || scss || sass || styl */
    columns: 12,
    /* number of grid columns */
    offset: '20px',
    /* gutter width px || % */
    mobileFirst: false,
    /* mobileFirst ? 'min-width' : 'max-width' */
    container: {
        maxWidth: '1150px',
        /* max-width оn very large screen */
        fields: '30px' /* side fields */
    },
    breakPoints: {
        lg: {
            width: '1100px',
            maxWidth: '900px',
            fields: '30px'
        },
        md: {
            width: '960px',
            maxWidth: '900px',
            fields: '30px'
        },
        sm: {
            width: '780px',
            fields: '15px' /* set fields only if you want to change container.fields */
        },
        xs: {
            width: '560px'
        }
        /*
        We can create any quantity of break points.

        some_name: {
            width: 'Npx',
            fields: 'N(px|%|rem)',
            offset: 'N(px|%|rem)'
        }
        */
    }
};

smartgrid('./src/instrum', settings);
