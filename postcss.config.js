module.exports = {
  plugins: {
    'autoprefixer': {browsers: ['last 2 version']},
    'postcss-focus': {}, // Добавляет focus к hover
    'postcss-size': {}, // Задавать сразу высоту и ширину
    'postcss-clearfix': {}, // clear: fix;
    'postcss-responsive-images': {}, // image-size: responsive;
    'postcss-center': {}, // Центрировать изображение
    'postcss-merge-longhand': {}, // Комбинирует margin, border, padding
    'postcss-assets': {}, // Находит путь к изображениям, инлайнит 'resolve', 'inline'
    'postcss-responsive-font': {}, // Делаем адаптивный текст
    'postcss-inline-media': {}, // Инлайнит медиа запросы
    // 'postcss-zindex': {}, // Приводит к нормальным числам z-index
    'postcss-discard-duplicates': {}, // Убирает дубли стилей
    //'cssnano': {}, // Собирает одинаковые стили
    'postcss-pxtorem': {propList: ['font', 'font-size', 'line-height', 'letter-spacing', 'padding', 'margin']} // px в rem
  }
}

// autoprefixer postcss-merge-longhand postcss-zindex postcss-discard-duplicates cssnano
